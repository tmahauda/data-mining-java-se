package universite.angers.master.info.data.mining.company;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;
import universite.angers.master.info.data.mining.company.model.Search;
import universite.angers.master.info.data.mining.company.read.FactoryBase;
import universite.angers.master.info.data.mining.company.read.FactorySearch;
import universite.angers.master.info.data.mining.company.solver.Solveable;
import universite.angers.master.info.data.mining.company.solver.Solver;
import universite.angers.master.info.data.mining.company.solver.branchandbound.SolverBranchAndBound;
import universite.angers.master.info.data.mining.company.solver.branchandbound.explorer.ExplorationWithFile;
import universite.angers.master.info.data.mining.company.solver.branchandbound.explorer.ExplorationWithFilePriority;
import universite.angers.master.info.data.mining.company.solver.branchandbound.explorer.ExplorationWithPile;
import universite.angers.master.info.data.mining.company.solver.glouton.SolverGlouton;

/**
 * Classe qui permet de rechercher des entreprises dans des bases selon des scénarios avec deux approches :
 * - Recherche avec un algo Glouton
 * - Recherche avec un algo B&B
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class Test {
	
	private static final Logger LOG = Logger.getLogger(Test.class);

	public static void main(String[] args) {
		
		//On construit d'abord les bases avec les entreprises
		FactoryBase.getInstance()
			.build("src/main/resources/data/bases", StandardCharsets.UTF_8);
		
		for(Base base : FactoryBase.getInstance().getBases().values()) {
			LOG.debug("Base : " + base.getName());
			LOG.debug("Cout : " + base.getCout());
			LOG.debug("Companies size : " + base.getCompanies().size());
		}
		
		for(Company company : FactoryBase.getInstance().getCompanies().values()) {
			LOG.debug("Company : " + company);
		}
		
		//Puis on construit les scénarios
		FactorySearch.getInstance()
			.build("src/main/resources/data/scenarios/scenario1", StandardCharsets.UTF_8)
			.build("src/main/resources/data/scenarios/scenario2", StandardCharsets.UTF_8)
			.build("src/main/resources/data/scenarios/scenario3", StandardCharsets.UTF_8);
	
		for(Search search : FactorySearch.getInstance().getSearchs().values()) {
			LOG.debug("Search : " + search.getName());
			LOG.debug("Borne max : " + search.getBorne());
			
			LOG.debug("Bases find size : " + search.getBasesFind().size());
			LOG.debug("Bases not find size : " + search.getBasesNotFind().size());
			LOG.debug("Bases use in search size : " + search.getBases().size());
			
			LOG.debug("Companies find size : " + search.getCompaniesFind().size());
			LOG.debug("Companies not find size : " + search.getCompaniesNotFind().size());
			LOG.debug("Companies use in search size : " + search.getCompanies().size());
		}
		
		/**
		 * Résolution avec un algorithme Glouton
		 */
		
		search(new SolverGlouton(), Base.getComparatorByNameIncreasing(), "Glouton");
		
		/**
		 * Résolution avec un algorithme B&B selon différentes stratégies d'exploration
		 */
		
		//Avec une pile
		
		//search(new SolverBranchAndBound(new ExplorationWithPile(), false), Base.getComparatorByCoutIncreasing(), "Branch and Bound avec une pile, cout croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithPile(), true), Base.getComparatorByCoutIncreasing(), "Branch and Bound avec une pile, cout croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithPile(), false), Base.getComparatorByCoutDescending(), "Branch and Bound avec une pile, cout décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithPile(), true), Base.getComparatorByCoutDescending(), "Branch and Bound avec une pile, cout décroissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithPile(), false), Base.getComparatorByNameIncreasing(), "Branch and Bound avec une pile, nom croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithPile(), true), Base.getComparatorByNameIncreasing(), "Branch and Bound avec une pile, nom croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithPile(), false), Base.getComparatorByNameDescending(), "Branch and Bound avec une pile, nom décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithPile(), true), Base.getComparatorByNameDescending(), "Branch and Bound avec une pile, nom décroissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithPile(), false), Base.getComparatorBySizeCompaniesIncreasing(), "Branch and Bound avec une pile, nombres d'entreprises croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithPile(), true), Base.getComparatorBySizeCompaniesIncreasing(), "Branch and Bound avec une pile, nombres d'entreprises croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithPile(), false), Base.getComparatorBySizeCompaniesDescending(), "Branch and Bound avec une pile, nombres d'entreprises décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithPile(), true), Base.getComparatorBySizeCompaniesDescending(), "Branch and Bound avec une pile, nombres d'entreprises décroissant et borne glouton");
		
		//Avec une file
		
		//search(new SolverBranchAndBound(new ExplorationWithFile(), false), Base.getComparatorByCoutIncreasing(), "Branch and Bound avec une file, cout croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFile(), true), Base.getComparatorByCoutIncreasing(), "Branch and Bound avec une file, cout croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFile(), false), Base.getComparatorByCoutDescending(), "Branch and Bound avec une file, cout décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFile(), true), Base.getComparatorByCoutDescending(), "Branch and Bound avec une file, cout décroissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFile(), false), Base.getComparatorByNameIncreasing(), "Branch and Bound avec une file, nom croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFile(), true), Base.getComparatorByNameIncreasing(), "Branch and Bound avec une file, nom croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFile(), false), Base.getComparatorByNameDescending(), "Branch and Bound avec une file, nom décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFile(), true), Base.getComparatorByNameDescending(), "Branch and Bound avec une file, nom décroissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFile(), false), Base.getComparatorBySizeCompaniesIncreasing(), "Branch and Bound avec une file, nombres d'entreprises croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFile(), true), Base.getComparatorBySizeCompaniesIncreasing(), "Branch and Bound avec une file, nombres d'entreprises croissant et borne glouton");
	
		//search(new SolverBranchAndBound(new ExplorationWithFile(), false), Base.getComparatorBySizeCompaniesDescending(), "Branch and Bound avec une file, nombres d'entreprises décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFile(), true), Base.getComparatorBySizeCompaniesDescending(), "Branch and Bound avec une file, nombres d'entreprises décroissant et borne glouton");
		
		//Avec une file prioritaire
		
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), false), Base.getComparatorByCoutIncreasing(), "Branch and Bound avec une file prioritaire, cout croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), true), Base.getComparatorByCoutIncreasing(), "Branch and Bound avec une file prioritaire, cout croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), false), Base.getComparatorByCoutDescending(), "Branch and Bound avec une file prioritaire, cout décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), true), Base.getComparatorByCoutDescending(), "Branch and Bound avec une file prioritaire, cout décroissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), false), Base.getComparatorByNameIncreasing(), "Branch and Bound avec une file prioritaire, nom croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), true), Base.getComparatorByNameIncreasing(), "Branch and Bound avec une file prioritaire, nom croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), false), Base.getComparatorByNameDescending(), "Branch and Bound avec une file prioritaire, nom décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), true), Base.getComparatorByNameDescending(), "Branch and Bound avec une file prioritaire, nom décroissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), false), Base.getComparatorBySizeCompaniesIncreasing(), "Branch and Bound avec une file prioritaire, nombres d'entreprises croissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), true), Base.getComparatorBySizeCompaniesIncreasing(), "Branch and Bound avec une file prioritaire, nombres d'entreprises croissant et borne glouton");
		
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), false), Base.getComparatorBySizeCompaniesDescending(), "Branch and Bound avec une file prioritaire, nombres d'entreprises décroissant et borne init");
		//search(new SolverBranchAndBound(new ExplorationWithFilePriority(), true), Base.getComparatorBySizeCompaniesDescending(), "Branch and Bound avec une file prioritaire, nombres d'entreprises décroissant et borne glouton");
	}

	/**
	 * Affiche les solutions trouvés pour chaque scnéario selon une stratégie
	 * @param solv la stratagie de résolution (glouton ou B&B)
	 * @param algo le nom de la stratégie
	 */
	private static void search(Solveable solv, Comparator<Base> compBase, String algo) {
		System.out.println("Résolution avec un algo " + algo);
		System.out.println();
		
		for(Search search : FactorySearch.getInstance().getSearchs().values()) {
			
			//On ordonne les bases pour savoir dans quelle ordre il faut les traiter
			Collections.sort(search.getBases(), compBase);
			
			System.out.println("Scénario " + search.getName());
			System.out.println();
			
			System.out.println("Cout total : " + search.getBorne());
			System.out.println();
			
			System.out.println("Les bases utilisées dans la recherche : ");
			for(Base base : search.getBases()) {
				System.out.println("\t"+base.getName() + " ; Cout = " + base.getCout() + " ; Nombre d'entreprises = " + base.getCompanies().size());
			}
			
			System.out.println();
			
			System.out.println("Les bases non utilisées dans la recherche : ");
			for(Base base : search.getBasesNotFind()) {
				System.out.println("\t"+base.getName() + " : " + base.getCout());
			}
			
			System.out.println();
			
			System.out.println("Les entreprises utilisées dans la recherche : ");
			for(Company company : search.getCompanies()) {
				System.out.println("\t"+company.getName());
			}
			
			System.out.println();
			
			System.out.println("Les entreprises non utilisées dans la recherche : ");
			for(Company company : search.getCompaniesNotFind()) {
				System.out.println("\t"+company.getName());
			}
			
			System.out.println();
			
			//On recherche la solution pour le scénario
			long start = System.currentTimeMillis();
			List<Solver> soluces = solv.findSoluce(search);
			long end = System.currentTimeMillis();
			
			Collections.sort(soluces);
			
			int i=1;
			for(Solver soluce : soluces) {
				System.out.println("Solution numéro " + i);
				
				System.out.println();
				
				System.out.println("Les bases trouvés : ");
				for(Base base : soluce.getBases()) {
					System.out.println("\t"+base.getName() + " : " + base.getCout());
				}
				
				System.out.println();
				
				System.out.println("Les entreprises trouvés : ");
				for(Company company : soluce.getCompanies()) {
					System.out.println("\t"+company.getName() + " trouvé dans le ou les bases suivantes : ");
					for(Base base : soluce.getBases()) {
						if(base.getCompanies().contains(company)) {
							System.out.println("\t\t"+base.getName() + " : " + base.getCout());
						}
					}
				}
				
				System.out.println();
				
				System.out.println("Cout de la solution : " + soluce.getCout());
				System.out.println("Temps de résolution : " + (end - start) + " ms");
				
				System.out.println();
				
				i++;
			}
			
			System.out.println();
		}
		
		System.out.println();
	}
	
}
