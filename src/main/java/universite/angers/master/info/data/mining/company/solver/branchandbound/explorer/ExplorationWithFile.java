package universite.angers.master.info.data.mining.company.solver.branchandbound.explorer;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import universite.angers.master.info.data.mining.company.solver.branchandbound.Node;

/**
 * Exploration avec une file
 * Une file est une liste d’éléments dans laquelle tout insertion se fait à une
 * extrémité de la file, appelé queue, et toute suppression se fait à une
 * extrémité de la file, appelée tête
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class ExplorationWithFile extends Exploration<LinkedList<Node>> {

	/**
     * Une file représenté par une structure LinkedList pour le parcours en largeur
     */
	public ExplorationWithFile() {
		super(new LinkedList<>());
	}
	
	@Override
	public Node remove() {
		//supprimer l’élément en tête de file
		try {
			Node n = this.nodes.remove();
			this.nodesExplore++;
			return n;
		} catch(NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean add(Node n) {
		//ajouter un élément en queue de file
		this.nodesCreate++;
		return this.nodes.add(n);
	}

	@Override
	public Node get() {
		return this.nodes.peek();
	}
}
