package universite.angers.master.info.data.mining.company.solver.glouton;

import java.util.ArrayList;
import java.util.List;

import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;

/**
 * Classe qui permet de lier une base avec les entreprises dedans
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class BaseCompanies implements Comparable<BaseCompanies> {
	
	/**
	 * base parmi les bases de recherches
	 */
	private Base base;
	
	/**
	 * les entreprises trouver dans base
	 */
	private List<Company> companies;
	
	/**
	 * le nombre d'entreprises trouver dans base
	 */
	private int count;
	
	public BaseCompanies() {
		this(null, new ArrayList<>(), 0);
	}
	
	public BaseCompanies(Base base, List<Company> companies, int count) {
		this.base = base;
		this.companies = companies;
		this.count = count;
	}

	/**
	 * @return the base
	 */
	public Base getBase() {
		return base;
	}

	/**
	 * @param base the base to set
	 */
	public void setBase(Base base) {
		this.base = base;
	}

	/**
	 * @return the companies
	 */
	public List<Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((companies == null) ? 0 : companies.hashCode());
		result = prime * result + count;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseCompanies other = (BaseCompanies) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (companies == null) {
			if (other.companies != null)
				return false;
		} else if (!companies.equals(other.companies))
			return false;
		if (count != other.count)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "solver [base=" + base.getName() + ", companies=" + companies + ", count=" + count + "]";
	}
 
	@Override
	public int compareTo(BaseCompanies b) {		
		return Integer.compare(this.count, b.count);
	}
}
