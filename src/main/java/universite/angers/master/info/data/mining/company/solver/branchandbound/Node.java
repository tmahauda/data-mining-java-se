package universite.angers.master.info.data.mining.company.solver.branchandbound;

import java.util.Set;
import java.util.HashSet;
import org.apache.log4j.Logger;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;

/**
 * Noeud binaire représentant l'arbre de recherche pour l'algo B&B
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class Node implements Comparable<Node> {
	
	private static final Logger LOG = Logger.getLogger(Node.class);
	
	/**
	 * Le noeud pere
	 */
	private Node father;
	
	/**
	 * Le noeud fils gauche
	 */
	private Node leftSon;
	
	/**
	 * Le noeud fils droit
	 */
	private Node rightSon;
	
	/**
	 * Les entreprises trouvées dans ce noeud
	 */
	private Set<Company> companiesFound;
	
	/**
	 * Les bases trouvées dans ne noeud
	 */
	private Set<Base> basesFound;
	
	/**
	 * Les bases qui reste à explorer
	 */
	private Set<Base> basesSearch;

	public Node(Node father, Node leftSon, Node rightSon) {
		this.father = father;
		LOG.debug("Father : " + this.father);
		
		this.leftSon = leftSon;
		LOG.debug("Fils gauche : " + this.leftSon);
		
		this.rightSon = rightSon;
		LOG.debug("Fils droit : " + this.rightSon);
		
		this.companiesFound = new HashSet<>();
		this.basesFound = new HashSet<>();
		this.basesSearch = new HashSet<>();
	}

	public Node() {
		this(null, null, null);
	}

	/**
	 * Somme des couts de toutes les bases
	 */
	public int costBases() {
		int cost = 0;
		if (this.father != null ) {
			cost+= this.father.costBases();
		}
		for (Base base : this.basesFound) {
			cost += base.getCout();
		}
		return cost;
	}
	
	/**
	 * Récupère toutes les entreprises trouvées depuis le noeud racine
	 * @return
	 */
	public Set<Company> getAllCompaniesFound() {
		Set<Company> companies = new HashSet<>();
		if (this.father != null) {
			companies.addAll(this.father.getAllCompaniesFound());
		}
		companies.addAll(this.companiesFound);
		return companies;
	}

	/**
	 * Récupère toutes les bases trouvées depuis le noeud racine
	 * @return
	 */
	public Set<Base> getAllBases() {
		Set<Base> bases = new HashSet<>();
		if (this.father != null) {
			bases.addAll(this.father.getAllBases());
		}
		bases.addAll(this.basesFound);
		return bases;
	}
	
	/**
	 * Récupère la profondeur de l'arbre
	 * @return
	 */
	public int getProfondeur() {
		if (this.father != null) {
			return this.father.getProfondeur() + 1;
		} 
		else return 1;
	}	
	
	/**
	 * @return the father
	 */
	public Node getFather() {
		return father;
	}

	/**
	 * @param father the father to set
	 */
	public void setFather(Node father) {
		this.father = father;
	}

	/**
	 * @return the leftSon
	 */
	public Node getLeftSon() {
		return leftSon;
	}

	/**
	 * @param leftSon the leftSon to set
	 */
	public void setLeftSon(Node leftSon) {
		this.leftSon = leftSon;
	}

	/**
	 * @return the rightSon
	 */
	public Node getRightSon() {
		return rightSon;
	}

	/**
	 * @param rightSon the rightSon to set
	 */
	public void setRightSon(Node rightSon) {
		this.rightSon = rightSon;
	}

	/**
	 * @return the companiesFound
	 */
	public Set<Company> getCompaniesFound() {
		return companiesFound;
	}

	/**
	 * @param companiesFound the companiesFound to set
	 */
	public void setCompaniesFound(Set<Company> companiesFound) {
		this.companiesFound = companiesFound;
	}

	/**
	 * @return the basesFound
	 */
	public Set<Base> getBasesFound() {
		return basesFound;
	}

	/**
	 * @param basesFound the basesFound to set
	 */
	public void setBasesFound(Set<Base> basesFound) {
		this.basesFound = basesFound;
	}

	/**
	 * @return the basesSearch
	 */
	public Set<Base> getBasesSearch() {
		return basesSearch;
	}

	/**
	 * @param basesSearch the basesSearch to set
	 */
	public void setBasesSearch(Set<Base> basesSearch) {
		this.basesSearch = basesSearch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((basesFound == null) ? 0 : basesFound.hashCode());
		result = prime * result + ((basesSearch == null) ? 0 : basesSearch.hashCode());
		result = prime * result + ((companiesFound == null) ? 0 : companiesFound.hashCode());
		result = prime * result + ((father == null) ? 0 : father.hashCode());
		result = prime * result + ((leftSon == null) ? 0 : leftSon.hashCode());
		result = prime * result + ((rightSon == null) ? 0 : rightSon.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (basesFound == null) {
			if (other.basesFound != null)
				return false;
		} else if (!basesFound.equals(other.basesFound))
			return false;
		if (companiesFound == null) {
			if (other.companiesFound != null)
				return false;
		} else if (!companiesFound.equals(other.companiesFound))
			return false;
		if (father == null) {
			if (other.father != null)
				return false;
		} else if (!father.equals(other.father))
			return false;
		if (leftSon == null) {
			if (other.leftSon != null)
				return false;
		} else if (!leftSon.equals(other.leftSon))
			return false;
		if (rightSon == null) {
			if (other.rightSon != null)
				return false;
		} else if (!rightSon.equals(other.rightSon))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Node [ companiesFound="
				+ companiesFound + ", basesFound=" + basesFound + ", basesSearch=" + basesSearch + "]";
	}

	@Override
	public int compareTo(Node n) {
		//On priorise le noeud qui a le cout minimum
		int cout = this.costBases() - n.costBases();
		
		//et le noeud qui a le plus d'entreprises couvertes
		int sizeCompanies = this.getAllCompaniesFound().size() + n.getAllCompaniesFound().size();
		
		return cout + sizeCompanies;
	}
}
