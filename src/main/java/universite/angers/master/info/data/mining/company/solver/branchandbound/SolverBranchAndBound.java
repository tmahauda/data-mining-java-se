package universite.angers.master.info.data.mining.company.solver.branchandbound;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;
import universite.angers.master.info.data.mining.company.model.Search;
import universite.angers.master.info.data.mining.company.solver.Solveable;
import universite.angers.master.info.data.mining.company.solver.Solver;
import universite.angers.master.info.data.mining.company.solver.branchandbound.explorer.Exploration;
import universite.angers.master.info.data.mining.company.solver.glouton.SolverGlouton;

/**
 * Classe qui permet de creer une liste de bases avec les entreprises trouvées à
 * l'interieur de chaque base
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class SolverBranchAndBound implements Solveable {
	
	private static final Logger LOG = Logger.getLogger(SolverBranchAndBound.class);

	/**
	 * Borne à ne pas dépasser lors de l'exploration
	 */
	private int borne;
	
	/**
	 * Profondeur courant dans l'arbre de la solution
	 */
	private int profondeur;
	
	/**
	 * L'arbre de recherche
	 */
	private Exploration<? extends Collection<Node>> nodes;
	
	/**
	 * Doit calculer la borne à partir de l'algo de Glouton ?
	 */
	private boolean calculBorneFromGlouton;
	
	public SolverBranchAndBound(Exploration<? extends Collection<Node>> nodes, boolean calculBorneFromGlouton) {
		this.calculBorneFromGlouton = calculBorneFromGlouton;
		this.nodes = nodes;
		this.borne = 0;
		this.profondeur = 0;
	}

	@Override
	public List<Solver> findSoluce (Search search) {
		LOG.debug("Search : " + search);
		
		//On nettoie tous les noeuds présent dans la structure
		this.nodes.clear();
		
		//On initialise la borne à ne pas dépasser
		//Soit on calcul la borne à partir du Glouton
		//Soit on prend le cout initial, à savoir la somme des couts de toutes les bases
		if(this.calculBorneFromGlouton) {
			SolverGlouton glouton = new SolverGlouton();
			List<Solver> soluces = glouton.findSoluce(search);
			if(soluces.isEmpty()) {
				this.borne = search.getBorne();
			} else {
				this.borne = soluces.get(0).getCout();
			}
		} else {
			this.borne = search.getBorne();
		}
		
		LOG.debug("Borne : " + this.borne);
		
		//La profondeur dans l'arbre
		this.profondeur = 0;
		
		//La liste des solutions. On peut avoir des solutions
		//qui ont le meme cout minimum
		List<Solver> soluces = new ArrayList<>();
		
		//Création du noeud racine
		Node root = new Node();

		Collection<Company> companiesSearch = search.getCompanies();
		LOG.debug("Companies search : " + companiesSearch);

		Collection<Base> baseSearch = search.getBases();
		LOG.debug("Bases search : " + baseSearch);

		//A la racine nous avons toutes les entreprises
		root.getBasesSearch().addAll(baseSearch);

		//On ajoute la racine dans l'arbre
		nodes.add(root);
		
		Node nodecurrent = null;
		
		while (!nodes.isEmpty()) {
			
			//On récupère le noeud en tete de structure
			nodecurrent = nodes.remove();
			
			//Si plus de noeud à explorer dans ce cas on quitte la boucle
			LOG.debug("Node current : " + nodecurrent);
			if (nodecurrent == null) break;
			
			/**
			 * Si la borne est supérieur on n'explore pas plus loin ce noeud
			 */
			
			int cost = nodecurrent.costBases();
			LOG.debug("Cout : " + cost);
			
			boolean overpassed = cost > this.borne;
			LOG.debug("Borne supérieur : " + overpassed);
			
			if (overpassed) continue;

			/**
			 * Si le noeud contient la solution dans ce cas on crée une solution et on passe aux noeuds suivants
			 */
			
			boolean containAll = nodecurrent.getAllCompaniesFound().containsAll(companiesSearch);
			LOG.debug("Noeud contient la solution : " + containAll);
			
			if (containAll) {
				
				//On met à jour la borne si le cout est strictement plus petit
				if(cost < this.borne) {
					this.borne = cost;
					
					//On supprime les solutions trouvés avant
					soluces.clear();
				}
				
				Solver solver = new Solver();
				this.profondeur = nodecurrent.getProfondeur();
				
				LOG.debug("All bases : " + nodecurrent.getAllBases());
				solver.setBases(new ArrayList<>(nodecurrent.getAllBases()));
		
				LOG.debug("All companies found : " + nodecurrent.getAllCompaniesFound());
				solver.setCompanies(new ArrayList<>(nodecurrent.getAllCompaniesFound()));
				
				solver.setCout(cost);
				LOG.debug("Cout : " + cost);
				
				soluces.add(solver);
				
				LOG.debug("Search soluce : " + search.getName());
				LOG.debug("Soluce : " + solver);
				
				continue;
			}

			/**
			 * Si plus de bases à explorer pour ce noeud on passe au noeud suivant
			 */
			
			boolean isempty = nodecurrent.getBasesSearch().isEmpty();
			LOG.debug("Bases à explorer : " + isempty);
			
			if (isempty) continue;

			Base base = (Base) nodecurrent.getBasesSearch().toArray()[0];

			//On regarde toutes les entreprises trouvés dans la base
			Collection<Company> companiesFound = searchCompaniesInBase(companiesSearch, base);
			LOG.debug("Companies found : " + companiesFound);

			//Le noeud fils droit qui va contenir aucune recherche de cette base = aucun cout
			Node noderight = new Node(nodecurrent, null, null);
			nodecurrent.setRightSon(noderight);
			
			//On crée le noeud fils gauche qui va contenir la recherche avec cette base = cout
			Node nodeleft = new Node(nodecurrent, null, null);
			nodecurrent.setLeftSon(nodeleft);
			
			//On place donc dans le fils gauche les entreprises trouvés avec la base
			nodeleft.getCompaniesFound().addAll(companiesFound);
			nodeleft.getBasesFound().add(base);

			/**
			 * On ajoute les bases a rechercher du noeud courant moins la base
			 * courante. En effet a chaque niveau de l'arbre on a une base de moins a
			 * traiter
			 */
			
			//Pour ce noeud gauche on récupère les bases à explorer actuellement
			nodeleft.getBasesSearch().addAll(nodecurrent.getBasesSearch());
			
			//Puis on supprime la base courante pour ce noeud gauche pour éviter de l'explorer à nouveau
			nodeleft.getBasesSearch().remove(base);
			
			//Pour ce noeud droit on récupère les bases à explorer actuellement
			noderight.getBasesSearch().addAll(nodecurrent.getBasesSearch());
			
			//Puis on supprime la base courante pour ce noeud droit pour éviter de l'explorer à nouveau
			noderight.getBasesSearch().remove(base);

			//On ajoute les noeuds droit et gauche
			this.nodes.add(noderight);
			this.nodes.add(nodeleft);
		}
		
		System.out.println("Nombre de bases crées : " + this.nodes.getNodesCreate());
		System.out.println("Nombre de bases explorées : " + this.nodes.getNodesExplore());
		System.out.println("Profondeur : " + this.profondeur);
		System.out.println("");
		
		return soluces;
	}
	
	/**
	 * Recherche les entreprises présent dans la base
	 * @param companiesSearch les entreprises à rechercher
	 * @param base
	 * @return les entreprises trouvés dans la base
	 */
	private Collection<Company> searchCompaniesInBase(Collection<Company> companiesSearch, Base base) {
		Collection<Company> comp = new ArrayList<>();
		for (Company company : companiesSearch) {
			if (base.getCompanies().contains(company)) {
				comp.add(company);
			}
		}
		return comp;
	}
}
