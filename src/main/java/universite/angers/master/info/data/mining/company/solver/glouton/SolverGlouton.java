package universite.angers.master.info.data.mining.company.solver.glouton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;
import universite.angers.master.info.data.mining.company.model.Search;
import universite.angers.master.info.data.mining.company.solver.Solveable;
import universite.angers.master.info.data.mining.company.solver.Solver;

/**
 * Classe qui permet de creer une liste de bases avec les entreprises trouvées à l'interieur de chaque base 
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class SolverGlouton implements Solveable {
	
	private static final Logger LOG = Logger.getLogger(SolverGlouton.class);

	/**
	 * le scenario
	 */
	private Search search;
	
	/**
	 * les bases avec leur info dedans
	 */
	private List<BaseCompanies> baseEntreprises;
	
	/**
	 * les entreprises avec les bases qui les contient
	 */
	private List<CompanyBases> companyBases;
	
	/**
	 * contient la solution unique
	 */
	private Solver solver;
	
	@Override
	public List<Solver> findSoluce (Search search) {
		this.search = new Search(search.getName());
		this.search.setBases(new ArrayList<>(search.getBases()));
		this.search.setCompanies(new ArrayList<>(search.getCompanies()));
		
		this.solver = new Solver();
		this.baseEntreprises = new ArrayList<>();
		this.companyBases = new ArrayList<>(); 
		
		this.getBasesWithFoundCompanies();
		
		// liste dentriprise dans une base (max) 
		BaseCompanies baseCompanies = this.getLast(this.baseEntreprises); 	
		
		for(BaseCompanies ba : this.baseEntreprises) {
			if(baseCompanies.getCount() == ba.getCount()) {
				if(baseCompanies.getBase().getCout() > ba.getBase().getCout()) {
					baseCompanies = ba;
				}
			}
		}
		
		LOG.debug(baseCompanies);
		
		this.solver.setCout(baseCompanies.getBase().getCout());		
		this.solver.setCompanies(baseCompanies.getCompanies());
		this.solver.getBases().add(baseCompanies.getBase());
		
		LOG.debug(this.solver.getCompanies());
		LOG.debug(this.solver.getCompanies().size());
		
		LOG.debug(this.search.getCompanies());
		LOG.debug(this.search.getCompanies().size());
		
		if(this.search.getCompanies().size() != this.solver.getCompanies().size()) {
		
			Collection<Company> companyLeft = this.removeAll(this.search.getCompanies(), this.solver.getCompanies());
			LOG.debug(companyLeft);
			
			Collection<CompanyBases> companiesLeft = this.getCompanyWithBasesInside(companyLeft);
							
			for(CompanyBases companyBases : companiesLeft) {					
				if(!this.solver.getBases().contains(companyBases.getBases().get(0))) {
					this.solver.getBases().add(companyBases.getBases().get(0));
					this.solver.getCompanies().add(companyBases.getCompany());
					this.solver.setCout(this.solver.getCout() + companyBases.getBases().get(0).getCout());
				}				
			}
								
			LOG.debug(this.solver.getBases());
			LOG.debug(this.solver.getCout());
								
		}
		
		for(Base base : this.solver.getBases()) {
			LOG.debug(base.getName() + " - " + base.getCout());
		}
		LOG.debug(this.solver.getCout());
		
		return Arrays.asList(this.solver);							
	}
	
	/**
	 * récuperer les bases avec les entreprises trouvées dedans
	 * @return une liste de BaseCompanies
	 */
	private List<BaseCompanies> getBasesWithFoundCompanies() {
		BaseCompanies baseCompanies;
		Integer count;
		List<Company> companies;
				
		for(Base base : this.search.getBases()) {				
			baseCompanies = new BaseCompanies();
			count = 0;					
			companies = new ArrayList<>();
			
			for(Company company : this.search.getCompanies()) {
				if(base.getCompanies().contains(company)) {	
					companies.add(company);	
					count++;
				}				
			}
			
			if(count!=0) {
				baseCompanies.setCount(count);
				baseCompanies.setBase(base);
				baseCompanies.setCompanies(companies);
				this.baseEntreprises.add(baseCompanies);	
			}			
		}		
		
		this.sortListBaseEntreprise(this.baseEntreprises);	
		
		for(BaseCompanies baCompanies : baseEntreprises) {
			LOG.debug(baCompanies.getBase().getName() + " - " + baCompanies.getCount());
		}
		
		return baseEntreprises;		
	}
	
	/**
	 * récuperer pour chauque entreprise restant les bases qui contient l'information 
	 * @param companyLeft les entreprises qui 
	 * @return une liste de CompanyBases
	 */
	private Collection<CompanyBases> getCompanyWithBasesInside(Collection<Company> companyLeft) {
		CompanyBases companyBase;
		
		for(Company company : companyLeft) {
			companyBase = new CompanyBases();
			companyBase.setCompany(company);
			
			for(Base base : this.search.getBases()) {		
				if(base.getCompanies().contains(company)) {	
					companyBase.addBase(base);					
				}
			}
			this.companyBases.add(companyBase);
		}			
		
		for(CompanyBases companBase : companyBases) {		
			Collections.sort(companBase.getBases());
			Base baseFirst = companBase.getBases().get(0);
		
			companBase.getBases().clear();
			companBase.getBases().add(baseFirst);			
		}
		
		
		return companyBases;		
	}
	
	/**
	 * Supprimer de la liste de recherches des entreprises
	 * les entreprises deja trouver dans des bases
	 * @param listOrigine les entreprises à rechercher
	 * @param listToRemove les entreprises trouvées	 
	 */
	private Collection<Company> removeAll(Collection<Company> listOrigine, Collection<Company> listToRemove) {
		Collection<Company> list;

	    listOrigine.removeAll(listToRemove);
	    list = listOrigine;

	    return list;
	}
	
	/**
	 * trier une liste de BaseCompanies	 
	 */
	private void sortListBaseEntreprise(List<BaseCompanies> listBasesCompanies) {
		Collections.sort(listBasesCompanies);
	}
	
	/**
	 * @return le dernier element de la liste	 
	 */
	private BaseCompanies getLast(List<BaseCompanies> listBasesCompanies) {
		return listBasesCompanies.get(listBasesCompanies.size() -1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baseEntreprises == null) ? 0 : baseEntreprises.hashCode());
		result = prime * result + ((companyBases == null) ? 0 : companyBases.hashCode());
		result = prime * result + ((search == null) ? 0 : search.hashCode());
		result = prime * result + ((solver == null) ? 0 : solver.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolverGlouton other = (SolverGlouton) obj;
		if (baseEntreprises == null) {
			if (other.baseEntreprises != null)
				return false;
		} else if (!baseEntreprises.equals(other.baseEntreprises))
			return false;
		if (companyBases == null) {
			if (other.companyBases != null)
				return false;
		} else if (!companyBases.equals(other.companyBases))
			return false;
		if (search == null) {
			if (other.search != null)
				return false;
		} else if (!search.equals(other.search))
			return false;
		if (solver == null) {
			if (other.solver != null)
				return false;
		} else if (!solver.equals(other.solver))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SolverGlouton [search=" + search + ", baseEntreprises=" + baseEntreprises + ", companyBases="
				+ companyBases + ", solver=" + solver + "]";
	}
}
