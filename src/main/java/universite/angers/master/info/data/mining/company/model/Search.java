package universite.angers.master.info.data.mining.company.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet d'effectuer un scénario de recherches
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class Search {

	/**
	 * Le nom de la recherhe
	 */
	private String name;
	
	/**
	 * La borne à respecter
	 */
	private int borne;
	
	/**
	 * Les bases dont il faut trouver les entreprises
	 */
	private List<Base> bases;
	
	/**
	 * Les bases qui ne servent à rien car ne contiennent aucune entreprise
	 */
	private List<Base> basesNotFind;
	
	/**
	 * Les bases trouver
	 */
	private List<Base> basesFind;
	
	/**
	 * Les entreprises à rechercher dans les bases
	 */
	private List<Company> companies;
	
	/**
	 * Les entreprises non trouver dans les bases
	 */
	private List<Company> companiesNotFind;
	
	/**
	 * Les entreprises trouver dans les bases
	 */
	private List<Company> companiesFind;
	
	public Search(String name) {
		this.name = name;
		this.basesFind = new ArrayList<>();
		this.basesNotFind = new ArrayList<>();
		this.bases = new ArrayList<>();
		this.companiesFind = new ArrayList<>();
		this.companiesNotFind = new ArrayList<>();
		this.companies = new ArrayList<>();
	}
	
	public void initCompaniesBases() {
		this.compagniesFindAndNotFind();
		this.basesFindAndNotFind();
		this.countBorneMax();
	}
	
	/**
	 * Recherche les entreprises qui ont été trouvés dans au moins une base. 
	 * Sinon l'entreprise n'est pas inclus dans la recherche.
	 */
	private void compagniesFindAndNotFind() {

		for(Company company : this.companiesFind) {
			boolean find = false;
			
			for(Base base : this.basesFind) {
				//Si l'entreprise est au moins présente dans une base dans 
				//ce cas on peut la rechercher
				if(base.getCompanies().contains(company)) {
					find = true;
					break;
				}
			}
			
			if(!find) {
				this.companiesNotFind.add(company);
			} else {
				this.companies.add(company);
			}
		}
	}
	
	/**
	 * Recherche les bases qui ont au moins une entreprise à rechercher. 
	 * Sinon la base n'est pas inclus dans la recherche
	 */
	private void basesFindAndNotFind() {

		for(Base base : this.basesFind) {
			boolean find = false;
			
			for(Company company : this.companies) {
				//Si la base contient au moins une entreprise à
				//rechercher dans ce cas on peut l'inclure dans la recherche
				if(base.getCompanies().contains(company)) {
					find = true;
					break;
				}
			}
		
			if(!find) {
				this.basesNotFind.add(base);
			} else {
				this.bases.add(base);
			}
		}
	}

	/**
	 * Calcul la borne maximum que l'on peut atteindre dans la recherche
	 * avec les bases utilisés
	 */
	private void countBorneMax() {
		int borneMax = 0;
		
		for(Base base : this.bases) {
			borneMax += base.getCout();
		}
		
		this.borne = borneMax;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the borne
	 */
	public int getBorne() {
		return borne;
	}

	/**
	 * @param borne the borne to set
	 */
	public void setBorne(int borne) {
		this.borne = borne;
	}

	/**
	 * @return the companies
	 */
	public List<Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	/**
	 * @return the bases
	 */
	public List<Base> getBases() {
		return bases;
	}

	/**
	 * @param bases the bases to set
	 */
	public void setBases(List<Base> bases) {
		this.bases = bases;
	}

	/**
	 * @return the basesNotFind
	 */
	public List<Base> getBasesNotFind() {
		return basesNotFind;
	}

	/**
	 * @param basesNotFind the basesNotFind to set
	 */
	public void setBasesNotFind(List<Base> basesNotFind) {
		this.basesNotFind = basesNotFind;
	}

	/**
	 * @return the basesFind
	 */
	public List<Base> getBasesFind() {
		return basesFind;
	}

	/**
	 * @param basesFind the basesFind to set
	 */
	public void setBasesFind(List<Base> basesFind) {
		this.basesFind = basesFind;
	}

	/**
	 * @return the companiesNotFind
	 */
	public List<Company> getCompaniesNotFind() {
		return companiesNotFind;
	}

	/**
	 * @param companiesNotFind the companiesNotFind to set
	 */
	public void setCompaniesNotFind(List<Company> companiesNotFind) {
		this.companiesNotFind = companiesNotFind;
	}

	/**
	 * @return the companiesFind
	 */
	public List<Company> getCompaniesFind() {
		return companiesFind;
	}

	/**
	 * @param companiesFind the companiesFind to set
	 */
	public void setCompaniesFind(List<Company> companiesFind) {
		this.companiesFind = companiesFind;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bases == null) ? 0 : bases.hashCode());
		result = prime * result + ((basesFind == null) ? 0 : basesFind.hashCode());
		result = prime * result + ((basesNotFind == null) ? 0 : basesNotFind.hashCode());
		result = prime * result + borne;
		result = prime * result + ((companies == null) ? 0 : companies.hashCode());
		result = prime * result + ((companiesFind == null) ? 0 : companiesFind.hashCode());
		result = prime * result + ((companiesNotFind == null) ? 0 : companiesNotFind.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Search other = (Search) obj;
		if (bases == null) {
			if (other.bases != null)
				return false;
		} else if (!bases.equals(other.bases))
			return false;
		if (basesFind == null) {
			if (other.basesFind != null)
				return false;
		} else if (!basesFind.equals(other.basesFind))
			return false;
		if (basesNotFind == null) {
			if (other.basesNotFind != null)
				return false;
		} else if (!basesNotFind.equals(other.basesNotFind))
			return false;
		if (borne != other.borne)
			return false;
		if (companies == null) {
			if (other.companies != null)
				return false;
		} else if (!companies.equals(other.companies))
			return false;
		if (companiesFind == null) {
			if (other.companiesFind != null)
				return false;
		} else if (!companiesFind.equals(other.companiesFind))
			return false;
		if (companiesNotFind == null) {
			if (other.companiesNotFind != null)
				return false;
		} else if (!companiesNotFind.equals(other.companiesNotFind))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Search [name=" + name + ", borne=" + borne + ", companies=" + companies + ", bases=" + bases
				+ ", basesNotFind=" + basesNotFind + ", basesFind=" + basesFind + ", companiesNotFind="
				+ companiesNotFind + ", companiesFind=" + companiesFind + "]";
	}
}
