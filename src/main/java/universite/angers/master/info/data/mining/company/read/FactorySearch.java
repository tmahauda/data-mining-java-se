package universite.angers.master.info.data.mining.company.read;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.data.mining.company.Util;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;
import universite.angers.master.info.data.mining.company.model.Search;

/**
 * Classe qui permet de fabriquer les recherches des entreprises dans les bases à partir de scénarios
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class FactorySearch implements Readable<Search> {

	private static final Logger LOG = Logger.getLogger(FactorySearch.class);
	
	/**
	 * L'unique instance
	 */
	private static FactorySearch instance;
	
	/**
	 * Les recherches à effectuer sur des entreprises stockées dans des bases
	 */
	private Map<String, Search> searchs;
	
	private FactorySearch() {
		this.searchs = new HashMap<>();
	}
	
	/**
	 * Le singleton qui assure l'unicité des recherches
	 * @return l'instance
	 */
	public static FactorySearch getInstance() {
		if(instance == null)
			instance = new FactorySearch();
		
		return instance;
	}
	
	/**
	 * Lit les scnéarios des bases à explorer
	 * @param path
	 * @param encodage
	 * @return
	 */
	private Map<String, Base> readScenariosBases(File file, Charset encodage) {
		Map<String, Base> bases = new HashMap<>();
		
		try (FileInputStream fis = new FileInputStream(file);
				InputStreamReader in = new InputStreamReader(fis, encodage);
				BufferedReader br = new BufferedReader(in)) {

			String line = null;

			while ((line = br.readLine()) != null) {
	
				//On supprime les éventuels espaces blancs
				line = line.trim();
				LOG.debug("Line : " + line);
				
				if(Util.isNullOrEmpty(line)) continue;
				
				Base base = FactoryBase.getInstance().getBase(line);
				LOG.debug("Base : " + base);
				
				if(base == null) continue;
				
				bases.put(base.getName(), base);
			}
			
			return bases;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return bases;
		}
	}
	
	/**
	 * Lit les scnéarios des bases à rechercher
	 * @param path
	 * @param encodage
	 * @return
	 */
	private Map<String, Company> readScenariosCompanies(File file, Charset encodage) {
		Map<String, Company> companies = new HashMap<>();
		
		try (FileInputStream fis = new FileInputStream(file);
				InputStreamReader in = new InputStreamReader(fis, encodage);
				BufferedReader br = new BufferedReader(in)) {

			String line = null;

			while ((line = br.readLine()) != null) {
				
				//On supprime les éventuels espaces blancs
				line = line.trim();
				LOG.debug("Line : " + line);
				
				if(Util.isNullOrEmpty(line)) continue;
				
				Company company = FactoryBase.getInstance().getCompany(line);
				LOG.debug("Company : " + company);
				
				if(company == null) continue;
				
				companies.put(company.getName(), company);
			}
			
			return companies;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return companies;
		}
	}

	@Override
	public Search read(File file, Charset encodage, String name) {
		LOG.debug("File : " + file);
		LOG.debug("Encodage : " + encodage);
		LOG.debug("Name : " + name);	
		
		if(file == null) return null;
		if(!file.isDirectory()) return null;
		if(file.listFiles().length != 2) return null;
		
		File fileScenariosBases = file.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().contains("bases");
			}
			
		})[0];
		
		LOG.debug("File scnearios bases : " + fileScenariosBases);
		
		Map<String, Base> bases = this.readScenariosBases(fileScenariosBases, encodage);
		LOG.debug("Bases : " + bases);
		
		File fileScenariosCompanies = file.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().contains("ent");
			}
			
		})[0];
		
		LOG.debug("File scnearios companies : " + fileScenariosCompanies);
		
		Map<String, Company> companies = this.readScenariosCompanies(fileScenariosCompanies, encodage);
		LOG.debug("Companies : " + companies);

		Search search = this.getOrNewSearch(name);
		search.setBasesFind(new ArrayList<>(bases.values()));
		search.setCompaniesFind(new ArrayList<>(companies.values()));
		search.initCompaniesBases();
		
		return search;
	}
	
	/**
	 * Récupère ou crée une nouvelle recherche en fonction du nom
	 * @param name
	 * @return
	 */
	public Search getOrNewSearch(String name) {
		if(this.searchs.containsKey(name)) {
			return this.searchs.get(name);	
		}
		else {
			Search search = new Search(name);
			this.searchs.put(name, search);
			return search;
		}
	}

	/**
	 * Construit une recherche complète à partir d'un fichier ou dossier
	 * @param path
	 * @param encodage
	 * @return
	 */
	public FactorySearch build(String path, Charset encodage) {
		File file = new File(path);
		this.read(file, encodage, file.getName());
		
		return this;
	}

	/**
	 * @return the searchs
	 */
	public Map<String, Search> getSearchs() {
		return searchs;
	}

	/**
	 * @param searchs the searchs to set
	 */
	public void setSearchs(Map<String, Search> searchs) {
		this.searchs = searchs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((searchs == null) ? 0 : searchs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FactorySearch other = (FactorySearch) obj;
		if (searchs == null) {
			if (other.searchs != null)
				return false;
		} else if (!searchs.equals(other.searchs))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FactorySearch [searchs=" + searchs + "]";
	}
}
