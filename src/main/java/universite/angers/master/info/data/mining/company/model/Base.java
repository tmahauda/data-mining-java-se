package universite.angers.master.info.data.mining.company.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Classe qui permet de stocker des infos sur des entreprises
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class Base implements Comparable<Base> {

	/**
	 * Le nom de la base
	 */
	private String name;
	
	/**
	 * Le cout pour interroger la base
	 */
	private int cout;
	
	/**
	 * La liste des entreprises disponibles dans la base
	 */
	private List<Company> companies;
	
	public Base(String name) {
		this.name = name;
		this.companies = new ArrayList<>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cout
	 */
	public int getCout() {
		return cout;
	}

	/**
	 * @param cout the cout to set
	 */
	public void setCout(int cout) {
		this.cout = cout;
	}

	/**
	 * @return the companies
	 */
	public List<Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((companies == null) ? 0 : companies.hashCode());
		result = prime * result + cout;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Base other = (Base) obj;
		if (companies == null) {
			if (other.companies != null)
				return false;
		} else if (!companies.equals(other.companies))
			return false;
		if (cout != other.cout)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Base [name=" + name + ", cout=" + cout + ", companies=" + companies + "]";
	}

	@Override
	public int compareTo(Base base) {
		return Integer.compare(this.cout, base.cout);
	}
	
	/**
	 * Récupère un comparateur par ordre croissant des noms de base
	 * @return
	 */
	public static Comparator<Base> getComparatorByNameIncreasing() {
		return new Comparator<Base>() {

			@Override
			public int compare(Base b1, Base b2) {
				return b1.getName().compareTo(b2.getName());
			}
		};
	}
	
	/**
	 * Récupère un comparateur par ordre décroissant des noms de base
	 * @return
	 */
	public static Comparator<Base> getComparatorByNameDescending() {
		return new Comparator<Base>() {

			@Override
			public int compare(Base b1, Base b2) {
				return b2.getName().compareTo(b1.getName());
			}
		};
	}
	
	/**
	 * Récupère un comparateur par ordre croissant du nombre d'entreprises présent
	 * @return
	 */
	public static Comparator<Base> getComparatorBySizeCompaniesIncreasing() {
		return new Comparator<Base>() {

			@Override
			public int compare(Base b1, Base b2) {
				return Integer.compare(b1.getCompanies().size(), b2.getCompanies().size());
			}
		};
	}
	
	/**
	 * Récupère un comparateur par ordre décroissant du nombre d'entreprises présent
	 * @return
	 */
	public static Comparator<Base> getComparatorBySizeCompaniesDescending() {
		return new Comparator<Base>() {

			@Override
			public int compare(Base b1, Base b2) {
				return Integer.compare(b2.getCompanies().size(), b1.getCompanies().size());
			}
		};
	}
	
	/**
	 * Récupère un comparateur par ordre croissant des cout
	 * @return
	 */
	public static Comparator<Base> getComparatorByCoutIncreasing() {
		return new Comparator<Base>() {

			@Override
			public int compare(Base b1, Base b2) {
				return Integer.compare(b1.getCout(), b2.getCout());
			}
		};
	}
	
	/**
	 * Récupère un comparateur par ordre décroissant des cout
	 * @return
	 */
	public static Comparator<Base> getComparatorByCoutDescending() {
		return new Comparator<Base>() {

			@Override
			public int compare(Base b1, Base b2) {
				return Integer.compare(b2.getCout(), b1.getCout());
			}
		};
	}
}
