package universite.angers.master.info.data.mining.company.solver.branchandbound.explorer;

import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import universite.angers.master.info.data.mining.company.solver.branchandbound.Node;

/**
 * Exploration avec une file prioritaire
 * Une file de priorité est une file dans laquelle l’ordre des éléments ne dépend
 * plus de leur date d’insertion mais d’une priorité entre eux
 * Pour définir la relation d’ordre entre les éléments, la classe Node implémente l’interface Comparable<E>, et en
 * particulier la méthode int compareTo(Node node)
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class ExplorationWithFilePriority extends Exploration<PriorityQueue<Node>> {

	/**
     * Une file prioritère par une structure PriorityQueue pour le parcours en A*
     */
	public ExplorationWithFilePriority() {
		super(new PriorityQueue<>());
	}
	
	@Override
	public Node remove() {
		//supprimer l’élément en tête de file
		try {
			Node n = this.nodes.remove();
			this.nodesExplore++;
			return n;
		} catch(NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean add(Node n) {
		//ajouter un élément en queue de file
		this.nodesCreate++;
		return this.nodes.add(n);
	}

	@Override
	public Node get() {
		return this.nodes.peek();
	}
}
