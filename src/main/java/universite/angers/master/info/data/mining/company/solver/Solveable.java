package universite.angers.master.info.data.mining.company.solver;

import java.util.List;
import universite.angers.master.info.data.mining.company.model.Search;

/**
 * Interface qui permet de résoudre une recherche pour retourner une liste de solutions
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public interface Solveable {

	/**
	 * Trouver le ou les solutions d'un scénario de recherche
	 * @param search
	 * @return
	 */
	public List<Solver> findSoluce (Search search);
}
