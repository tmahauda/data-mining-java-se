package universite.angers.master.info.data.mining.company.solver.branchandbound.explorer;

import java.util.Collection;
import universite.angers.master.info.data.mining.company.solver.branchandbound.Node;

/** 
 * Exploration qui permet de stocker les noeuds dans une structure de son choix (pile, file, file prioritaire)
 * et de compteur le nombre de noeuds explorés et créés.
 *
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public abstract class Exploration<T extends Collection<Node>> implements Explorable {

	/**
	 * Les noeuds
	 */
	protected T nodes;
	
	/**
	 * Nombre de noeuds explorés
	 */
	protected int nodesExplore;
	
	/**
	 * Nombre de noeuds crées
	 */
	protected int nodesCreate;
	
	public Exploration(T nodes) {
		this.nodes = nodes;
		this.nodesExplore = 0;
		this.nodesCreate = 0;
	}

	@Override
	public int size() {
		return this.nodes.size();
	}

	@Override
	public boolean isEmpty() {
		return this.nodes.isEmpty();
	}
	
	@Override
	public void clear() {
		this.nodes.clear();
	}
	
	/**
	 * @return the nodes
	 */
	public T getNodes() {
		return nodes;
	}

	/**
	 * @param nodes the nodes to set
	 */
	public void setNodes(T nodes) {
		this.nodes = nodes;
	}

	/**
	 * @return the nodesExplore
	 */
	public int getNodesExplore() {
		return nodesExplore;
	}

	/**
	 * @param nodesExplore the nodesExplore to set
	 */
	public void setNodesExplore(int nodesExplore) {
		this.nodesExplore = nodesExplore;
	}

	/**
	 * @return the nodesCreate
	 */
	public int getNodesCreate() {
		return nodesCreate;
	}

	/**
	 * @param nodesCreate the nodesCreate to set
	 */
	public void setNodesCreate(int nodesCreate) {
		this.nodesCreate = nodesCreate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodes == null) ? 0 : nodes.hashCode());
		result = prime * result + nodesCreate;
		result = prime * result + nodesExplore;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exploration<?> other = (Exploration<?>) obj;
		if (nodes == null) {
			if (other.nodes != null)
				return false;
		} else if (!nodes.equals(other.nodes))
			return false;
		if (nodesCreate != other.nodesCreate)
			return false;
		if (nodesExplore != other.nodesExplore)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Exploration [nodes=" + nodes + ", nodesExplore=" + nodesExplore + ", nodesCreate=" + nodesCreate + "]";
	}
}
