package universite.angers.master.info.data.mining.company.solver;

import java.util.ArrayList;
import java.util.Collection;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;

/**
 * Classe qui permet de trouver une solution à des recherches
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class Solver implements Comparable<Solver> {
	
	/**
	 * les bases qui contient les entreprises à chercher
	 */
	private Collection<Base> bases;
	
	/**
	 * les entreprises trouvées
	 */
	private Collection<Company> companies;
	
	/**
	 * le cout de recherche
	 */
	private int cout;
	
	public Solver() {		
		this.bases = new ArrayList<>();
		this.companies = new ArrayList<>();
		this.cout = 0;
	}

	/**
	 * @return the bases
	 */
	public Collection<Base> getBases() {
		return bases;
	}

	/**
	 * @param bases the bases to set
	 */
	public void setBases(Collection<Base> bases) {
		this.bases = bases;
	}

	/**
	 * @return the companies
	 */
	public Collection<Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(Collection<Company> companies) {
		this.companies = companies;
	}

	/**
	 * @return the cout
	 */
	public int getCout() {
		return cout;
	}

	/**
	 * @param cout the cout to set
	 */
	public void setCout(int cout) {
		this.cout = cout;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bases == null) ? 0 : bases.hashCode());
		result = prime * result + ((companies == null) ? 0 : companies.hashCode());
		result = prime * result + cout;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Solver other = (Solver) obj;
		if (bases == null) {
			if (other.bases != null)
				return false;
		} else if (!bases.equals(other.bases))
			return false;
		if (companies == null) {
			if (other.companies != null)
				return false;
		} else if (!companies.equals(other.companies))
			return false;
		if (cout != other.cout)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Solver [bases=" + bases + ", companies=" + companies + ", cout=" + cout + "]";
	}

	@Override
	public int compareTo(Solver s) {
		return Integer.compare(this.cout, s.cout);
	}
}
