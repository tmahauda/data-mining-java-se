package universite.angers.master.info.data.mining.company.solver.branchandbound.explorer;

import universite.angers.master.info.data.mining.company.solver.branchandbound.Node;

/** 
 * L'interface Explorable représente la collection des noeuds générés mais non encore explorés utilisée par l'algorithme de recherche B&B
 *
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public interface Explorable {

	/**
     * Méthode qui permet de supprimer et de récupérer le noeud en tête de la structure
     * @return noeud en tête
     */
	public Node remove();
	
	/**
     * Méthode qui permet d'ajouter un noeud en tête de la structure
     * @param n le noeud à ajouter
     */
	public boolean add(Node n);
	
	/**
     * Méthode qui permet de récupérer le noeud en tête de structure sans pour autant la supprimer
     * @return le noeud en tête
     */
	public Node get();
	
	/**
     * Méthode qui permet de récupérer la taille de la structure
     * @return la taille
     */
	public int size();
	
	/**
     * Méthode qui permet de savoir si la structure est vide ou non
     * @return la taille
     */
	public boolean isEmpty();
	
	/**
	 * Méthode qui permet de supprimer tous les noeuds dans la structure
	 */
	public void clear();
}
