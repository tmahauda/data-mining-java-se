package universite.angers.master.info.data.mining.company.read;

import java.io.File;
import java.nio.charset.Charset;

/**
 * Interface qui permet de lire des fichier pour construire un objet T
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 *
 * @param <T> l'objet à construire
 */
public interface Readable<T> {

	/**
	 * Lire le contenu d'un fichier pour fabriquer un objet T
	 * @param file le fichier
	 * @param encodage l'encodage pour lire (UTF-8)
	 * @param name le nom de l'objet T
	 * @return
	 */
	public T read(File file, Charset encodage, String name);
}
