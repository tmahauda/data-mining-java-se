package universite.angers.master.info.data.mining.company.read;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.data.mining.company.Util;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;

/**
 * Classe qui permet de fabriquer les bases contenant des infos sur des entreprises
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class FactoryBase implements Readable<Base> {

	private static final Logger LOG = Logger.getLogger(FactoryBase.class);
	
	/**
	 * L'unique instance
	 */
	private static FactoryBase instance;
	
	/**
	 * Toutes les bases disponibles
	 */
	private Map<String, Base> bases;
	
	/**
	 * Toutes les entreprises disponibles
	 */
	private Map<String, Company> companies;
	
	private FactoryBase() {
		this.bases = new HashMap<>();
		this.companies = new HashMap<>();
	}
	
	/**
	 * Le singleton qui assure l'unicité des recherches
	 * @return l'instance
	 */
	public static FactoryBase getInstance() {
		if(instance == null)
			instance = new FactoryBase();
		
		return instance;
	}
	
	/**
	 * Lit le contenu d'un fichier pour construire une base complète
	 * @param path
	 * @param encodage
	 * @return
	 */
	public Base read(File file, Charset encodage, String name) {
		LOG.debug("File : " + file);
		LOG.debug("Encodage : " + encodage);
		LOG.debug("Name : " + name);
		
		try (FileInputStream fis = new FileInputStream(file);
				InputStreamReader in = new InputStreamReader(fis, encodage);
				BufferedReader br = new BufferedReader(in)) {

			String line = null;

			Base base = this.getOrNewBase(name);
			LOG.debug("Base : " + base);
			
			while ((line = br.readLine()) != null) {
				
				//On supprime les éventuels espaces blancs
				line = line.trim();
				LOG.debug("Line : " + line);
				
				if(Util.isNullOrEmpty(line)) continue;
				
				//Si c'est un chiffre dans ce cas c'est le cout de la base
				if(this.isDigit(line)) {
					int cout = Integer.parseInt(line);
					base.setCout(cout);
				} else {
					Company company = this.getOrNewCompany(line);
					LOG.debug("Company : " + company);
					
					base.getCompanies().add(company);
				}
			}
			
			return base;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return null;
		}
	}
	
	/**
	 * Vérifie si une ligne contient uniquement des chiffres : c'est donc le cout de la base
	 * @param line
	 * @return
	 */
	private boolean isDigit(String line) {
		try {
			Integer.parseInt(line);	
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	/**
	 * Récupère uniquement une base par son nom
	 * @param name
	 * @return
	 */
	public Base getBase(String name) {
		if(this.bases.containsKey(name))
			return this.bases.get(name);
		else
			return null;
	}
	
	/**
	 * Récupère ou crée une nouvelle base en fonction du nom
	 * @param name
	 * @return
	 */
	private Base getOrNewBase(String name) {
		if(this.bases.containsKey(name)) {
			return this.bases.get(name);	
		}
		else {
			Base base = new Base(name);
			this.bases.put(name, base);
			return base;
		}
	}
	
	/**
	 * Récupère uniquement une entreprise par son nom
	 * @param name
	 * @return
	 */
	public Company getCompany(String name) {
		if(this.companies.containsKey(name))
			return this.companies.get(name);
		else
			return null;
	}
	
	/**
	 * Récupère ou crée une nouvelle entreprise en fonction du nom
	 * @param name
	 * @return
	 */
	private Company getOrNewCompany(String name) {
		if(this.companies.containsKey(name)) {
			return this.companies.get(name);	
		}
		else {
			Company company = new Company(name);
			this.companies.put(name, company);
			return company;
		}
	}
	
	/**
	 * Construit une base complète à partir d'un fichier ou dossier
	 * @param path
	 * @param encodage
	 * @return
	 */
	public FactoryBase build(String path, Charset encodage) {
		File file = new File(path);
		return this.build(file, encodage);
	}
	
	/**
	 * Construit une base complète à partir d'un fichier ou dossier
	 * @param path
	 * @param encodage
	 * @return
	 */
	public FactoryBase build(File file, Charset encodage) {
		LOG.debug("File : " + file);
		LOG.debug("Encodage : " + encodage);
		
		
		if(file.isDirectory()) {
			for(File subFile : file.listFiles()) {
				LOG.debug("Path sub file : " + subFile.getPath());
				this.build(subFile, encodage);
			}
		} else {
			this.read(file, encodage, file.getName());	
		}
		
		return this;
	}

	/**
	 * @return the bases
	 */
	public Map<String, Base> getBases() {
		return bases;
	}

	/**
	 * @param bases the bases to set
	 */
	public void setBases(Map<String, Base> bases) {
		this.bases = bases;
	}

	/**
	 * @return the companies
	 */
	public Map<String, Company> getCompanies() {
		return companies;
	}

	/**
	 * @param companies the companies to set
	 */
	public void setCompanies(Map<String, Company> companies) {
		this.companies = companies;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bases == null) ? 0 : bases.hashCode());
		result = prime * result + ((companies == null) ? 0 : companies.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FactoryBase other = (FactoryBase) obj;
		if (bases == null) {
			if (other.bases != null)
				return false;
		} else if (!bases.equals(other.bases))
			return false;
		if (companies == null) {
			if (other.companies != null)
				return false;
		} else if (!companies.equals(other.companies))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FactoryBase [bases=" + bases + ", companies=" + companies + "]";
	}
}
