package universite.angers.master.info.data.mining.company;

/**
 * Classe utilitaire de l'application
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class Util {

	private Util() {
		
	}
	
	/**
	 * Vérifier si une chaine est vide
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }
}
