package universite.angers.master.info.data.mining.company.solver.branchandbound.explorer;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import universite.angers.master.info.data.mining.company.solver.branchandbound.Node;

/**
 * Exploration avec une pile
 * Une pile est une liste d’éléments dans laquelle tout insertion et toute
 * suppression se fait à la même extrémité de la pile, appelée sommet
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class ExplorationWithPile extends Exploration<LinkedList<Node>> {

	/**
     * Une pile représenté par une structure LinkedList pour le parcours en profondeur
     */
	public ExplorationWithPile() {
		super(new LinkedList<>());
	}
	
	@Override
	public Node remove() {
		//dépiler l’élément en sommet de pile
		try {
			Node n = this.nodes.removeFirst();
			this.nodesExplore++;
			return n;
		} catch(NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public boolean add(Node n) {
		//empiler un élément
		this.nodes.addFirst(n);
		this.nodesCreate++;
		return true;
	}
	
	@Override
	public Node get() {
		return this.nodes.getFirst();
	}
}
