package universite.angers.master.info.data.mining.company.solver.glouton;

import java.util.ArrayList;
import java.util.List;
import universite.angers.master.info.data.mining.company.model.Base;
import universite.angers.master.info.data.mining.company.model.Company;

/**
 * Classe qui permet de lier une entreprise avec les bases qui la contiento
 * 
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @organisation : Master Informatique M1 à l'universite d'Angers
 * @date 13/04/2020
 * @version 1.0
 */
public class CompanyBases{
	
	/**
	 * Entreprise parmi les entreprises de recherche
	 */
	private Company company;
	
	/**
	 * Les bases qui contient l'entreprise company
	 */
	private List<Base> bases;
		
	
	public CompanyBases() {
		this(null, new ArrayList<>());
	}
	
	public CompanyBases(Company company, List<Base> bases) {
		this.company = company;
		this.bases = bases;
	}	
	
	/**
	 * ajouter une base à la liste des bases
	 * @param base
	 * @return true si la base est bien ajouté
	 * sinon false
	 */
	public boolean addBase(Base base) {
		return bases.add(base);
	}
	
	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the bases
	 */
	public List<Base> getBases() {
		return bases;
	}

	/**
	 * @param bases the bases to set
	 */
	public void setBases(List<Base> bases) {
		this.bases = bases;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bases == null) ? 0 : bases.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompanyBases other = (CompanyBases) obj;
		if (bases == null) {
			if (other.bases != null)
				return false;
		} else if (!bases.equals(other.bases))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CompanyBases [company=" + company + ", bases=" + bases + "]";
	}
}
