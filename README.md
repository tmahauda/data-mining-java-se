# Data Mining

<div align="center">
<img width="300" height="400" src="data_mining.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Optimisation Combinatoire" durant l'année 2019-2020 avec un groupe de trois personnes. \
Cette application permet de rechercher des informations concernant des entreprises sur un ensemble de BD avec des algorithmes de recherche Glouton ET branch & bound.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de trois étudiants de l'université d'Angers :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Anas TAGUENITI : atagueniti@etud.univ-angers.fr ;
- Mohamed OUHIRRA : mouhirra@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Jean-Philippe HAMIEZ : jean-philippe.hamiez@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Optimisation Combinatoire" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 13/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Branch & Bound ;
- Glouton.

## Objectif

Rechercher des informations concernant des entreprises sur un ensemble de BD avec des algorithmes de recherche Glouton ET branch & bound (au moins une approche minimum).

Implémenter deux algos de recherche :
- Glouton
    - Chaque passage d'un noeud à autre noeud est un choix d'une BD en rajoutant le cout, la profondeur ;
    - Approche primale : choisir une entreprise E puis une base B contenant des infos ;
    - Aproche duale : choisir une base B, puis une autre base. \
--> Pas de notion de retour. On avance. Pas de borne.
- Branch & bound (deux approches possibles)
    - Arbres binaires ;
    - Arbre n-aires ;
    - Chaque passage d'un noeud à autre noeud est un choix d'entreprise en rajoutant le cout, la profondeur. \
--> On peut retourner en arrière si une branche n'est pas pertinente.
- Faire une comparaison de temps de résolutions entre les deux ;
- Trouver la raison pourquoi les deux algos ?

## Fonctionnement

On a deux entrées :
- Ce qu'on veut chercher (scénarios d'entreprises) ;
- Dans quelles bases on va chercher les informations concernant les entreprises (scénarios BD).

Questions :
- Doit-on prendre l'ensembles entreprises pour rechercher dans les bases ou rechercher une par une (itération) ?
- Qui fournit le cout de départ S ?
	C'est le glouton
- Pourquoi on a besoin de deux algorithmes?
	En fait l'algorithme de Glouton nous permet de trouver une solution mais pas formcement une solution optimale. \
	C'est pour cela on l'utilise avec d'autre algorithme comme base de comparaison.

S = C'est UNE borne parmis plusieurs
- Somme de tous les coups des bases
- Initialisé à 0

Pas de programmation dynamique

## Organisation

- Les sources se trouvent dans le dossier src/main/java
- Les tests se trouvent dans le dossier src/test/java

## Exécution

Pour démarrer le programme, se rendre dans le projet où il y a le pom.xml et éxécuter la ligne de commande suivante :
mvn exec:java

Vous obtiendrez le résultat suivant :

Résolution avec un algo Glouton

Scénario scenario1

Cout total : 1248

Les bases utilisées dans la recherche : 
	Base_01.txt ; Cout = 87 ; Nombre d'entreprises = 29
	Base_02.txt ; Cout = 26 ; Nombre d'entreprises = 85
	Base_03.txt ; Cout = 43 ; Nombre d'entreprises = 23
	Base_04.txt ; Cout = 74 ; Nombre d'entreprises = 15
	Base_05.txt ; Cout = 82 ; Nombre d'entreprises = 75
	Base_06.txt ; Cout = 81 ; Nombre d'entreprises = 57
	Base_07.txt ; Cout = 20 ; Nombre d'entreprises = 56
	Base_08.txt ; Cout = 75 ; Nombre d'entreprises = 27
	Base_09.txt ; Cout = 58 ; Nombre d'entreprises = 19
	Base_10.txt ; Cout = 31 ; Nombre d'entreprises = 41
	Base_11.txt ; Cout = 84 ; Nombre d'entreprises = 20
	Base_12.txt ; Cout = 69 ; Nombre d'entreprises = 15
	Base_13.txt ; Cout = 36 ; Nombre d'entreprises = 29
	Base_14.txt ; Cout = 9 ; Nombre d'entreprises = 25
	Base_16.txt ; Cout = 53 ; Nombre d'entreprises = 51
	Base_17.txt ; Cout = 48 ; Nombre d'entreprises = 34
	Base_18.txt ; Cout = 80 ; Nombre d'entreprises = 31
	Base_20.txt ; Cout = 78 ; Nombre d'entreprises = 24
	Base_21.txt ; Cout = 83 ; Nombre d'entreprises = 21
	Base_22.txt ; Cout = 88 ; Nombre d'entreprises = 65
	Base_23.txt ; Cout = 43 ; Nombre d'entreprises = 46

Les bases non utilisées dans la recherche : 
	Base_15.txt : 86
	Base_19.txt : 42

Les entreprises utilisées dans la recherche : 
	MEDASYS INFRASTRUCTURE SERVICES
	ADONIX APPLICATIONS ET SERVICES
	CONTINUITY SERVICES FRANCE
	FRANCE TELECOM EXPERTISE ET SERVICE
	EDS GLOBAL FIELD SERVICES FRANCE
	SERVICES LOGICIELS INTEGRATION BOURSIERE
	AMADEUS FRANCE SERVICES
	ALTEN SYSTEMES D'INFORMATIONS NORD
	ASSISTANCE-CONSEIL-TECHNIQUE-INFORMATIQU
	TRANSICIEL INFOGERANCE SYSTEMES RESEAUX

Les entreprises non utilisées dans la recherche : 

Solution numéro 1

Les bases trouvés : 
	Base_16.txt : 53
	Base_10.txt : 31

Les entreprises trouvés : 
	MEDASYS INFRASTRUCTURE SERVICES trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
		Base_10.txt : 31
	ADONIX APPLICATIONS ET SERVICES trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
	CONTINUITY SERVICES FRANCE trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
	FRANCE TELECOM EXPERTISE ET SERVICE trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
	EDS GLOBAL FIELD SERVICES FRANCE trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
	SERVICES LOGICIELS INTEGRATION BOURSIERE trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
	AMADEUS FRANCE SERVICES trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
	ASSISTANCE-CONSEIL-TECHNIQUE-INFORMATIQU trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
		Base_10.txt : 31
	TRANSICIEL INFOGERANCE SYSTEMES RESEAUX trouvé dans le ou les bases suivantes : 
		Base_16.txt : 53
		Base_10.txt : 31
	ALTEN SYSTEMES D'INFORMATIONS NORD trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31

Cout de la solution : 84
Temps de résolution : 4 ms


Scénario scenario2

Cout total : 955

Les bases utilisées dans la recherche : 
	Base_01.txt ; Cout = 87 ; Nombre d'entreprises = 29
	Base_02.txt ; Cout = 26 ; Nombre d'entreprises = 85
	Base_03.txt ; Cout = 43 ; Nombre d'entreprises = 23
	Base_05.txt ; Cout = 82 ; Nombre d'entreprises = 75
	Base_06.txt ; Cout = 81 ; Nombre d'entreprises = 57
	Base_07.txt ; Cout = 20 ; Nombre d'entreprises = 56
	Base_08.txt ; Cout = 75 ; Nombre d'entreprises = 27
	Base_09.txt ; Cout = 58 ; Nombre d'entreprises = 19
	Base_10.txt ; Cout = 31 ; Nombre d'entreprises = 41
	Base_11.txt ; Cout = 84 ; Nombre d'entreprises = 20
	Base_12.txt ; Cout = 69 ; Nombre d'entreprises = 15
	Base_13.txt ; Cout = 36 ; Nombre d'entreprises = 29
	Base_16.txt ; Cout = 53 ; Nombre d'entreprises = 51
	Base_18.txt ; Cout = 80 ; Nombre d'entreprises = 31
	Base_19.txt ; Cout = 42 ; Nombre d'entreprises = 27
	Base_22.txt ; Cout = 88 ; Nombre d'entreprises = 65

Les bases non utilisées dans la recherche : 
	Base_14.txt : 9
	Base_20.txt : 78

Les entreprises utilisées dans la recherche : 
	ALSTOM INFORMATION TECHNOLOGY CENTRE
	ASSISTANCE GENIE LOGICIEL
	ATOS ORIGIN INFOGERANCE
	ARINSO FRANCE
	ALCANET INTERNATIONAL
	BASE D INFORMATIONS LEGALES

Les entreprises non utilisées dans la recherche : 

Solution numéro 1

Les bases trouvés : 
	Base_02.txt : 26
	Base_10.txt : 31

Les entreprises trouvés : 
	ASSISTANCE GENIE LOGICIEL trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26
	ATOS ORIGIN INFOGERANCE trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26
		Base_10.txt : 31
	ARINSO FRANCE trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26
	ALCANET INTERNATIONAL trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26
	ALSTOM INFORMATION TECHNOLOGY CENTRE trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31

Cout de la solution : 57
Temps de résolution : 2 ms


Scénario scenario3

Cout total : 473

Les bases utilisées dans la recherche : 
	Base_01.txt ; Cout = 87 ; Nombre d'entreprises = 29
	Base_02.txt ; Cout = 26 ; Nombre d'entreprises = 85
	Base_06.txt ; Cout = 81 ; Nombre d'entreprises = 57
	Base_07.txt ; Cout = 20 ; Nombre d'entreprises = 56
	Base_10.txt ; Cout = 31 ; Nombre d'entreprises = 41
	Base_13.txt ; Cout = 36 ; Nombre d'entreprises = 29
	Base_14.txt ; Cout = 9 ; Nombre d'entreprises = 25
	Base_16.txt ; Cout = 53 ; Nombre d'entreprises = 51
	Base_19.txt ; Cout = 42 ; Nombre d'entreprises = 27
	Base_22.txt ; Cout = 88 ; Nombre d'entreprises = 65

Les bases non utilisées dans la recherche : 
	Base_21.txt : 83
	Base_03.txt : 43

Les entreprises utilisées dans la recherche : 
	ADP FRANCE
	A.G.F. - INFORMATIQUE
	AIRIAL CONSEIL
	ALDATA SOLUTION

Les entreprises non utilisées dans la recherche : 

Solution numéro 1

Les bases trouvés : 
	Base_01.txt : 87
	Base_07.txt : 20
	Base_14.txt : 9

Les entreprises trouvés : 
	AIRIAL CONSEIL trouvé dans le ou les bases suivantes : 
		Base_01.txt : 87
	ALDATA SOLUTION trouvé dans le ou les bases suivantes : 
		Base_01.txt : 87
	ADP FRANCE trouvé dans le ou les bases suivantes : 
		Base_07.txt : 20
	A.G.F. - INFORMATIQUE trouvé dans le ou les bases suivantes : 
		Base_14.txt : 9

Cout de la solution : 116
Temps de résolution : 2 ms



Résolution avec un algo Branch and Bound avec une file prioritaire, cout décroissant et borne glouton

Scénario scenario1

Cout total : 1248

Les bases utilisées dans la recherche : 
	Base_22.txt ; Cout = 88 ; Nombre d'entreprises = 65
	Base_01.txt ; Cout = 87 ; Nombre d'entreprises = 29
	Base_11.txt ; Cout = 84 ; Nombre d'entreprises = 20
	Base_21.txt ; Cout = 83 ; Nombre d'entreprises = 21
	Base_05.txt ; Cout = 82 ; Nombre d'entreprises = 75
	Base_06.txt ; Cout = 81 ; Nombre d'entreprises = 57
	Base_18.txt ; Cout = 80 ; Nombre d'entreprises = 31
	Base_20.txt ; Cout = 78 ; Nombre d'entreprises = 24
	Base_08.txt ; Cout = 75 ; Nombre d'entreprises = 27
	Base_04.txt ; Cout = 74 ; Nombre d'entreprises = 15
	Base_12.txt ; Cout = 69 ; Nombre d'entreprises = 15
	Base_09.txt ; Cout = 58 ; Nombre d'entreprises = 19
	Base_16.txt ; Cout = 53 ; Nombre d'entreprises = 51
	Base_17.txt ; Cout = 48 ; Nombre d'entreprises = 34
	Base_03.txt ; Cout = 43 ; Nombre d'entreprises = 23
	Base_23.txt ; Cout = 43 ; Nombre d'entreprises = 46
	Base_13.txt ; Cout = 36 ; Nombre d'entreprises = 29
	Base_10.txt ; Cout = 31 ; Nombre d'entreprises = 41
	Base_02.txt ; Cout = 26 ; Nombre d'entreprises = 85
	Base_07.txt ; Cout = 20 ; Nombre d'entreprises = 56
	Base_14.txt ; Cout = 9 ; Nombre d'entreprises = 25

Les bases non utilisées dans la recherche : 
	Base_15.txt : 86
	Base_19.txt : 42

Les entreprises utilisées dans la recherche : 
	MEDASYS INFRASTRUCTURE SERVICES
	ADONIX APPLICATIONS ET SERVICES
	CONTINUITY SERVICES FRANCE
	FRANCE TELECOM EXPERTISE ET SERVICE
	EDS GLOBAL FIELD SERVICES FRANCE
	SERVICES LOGICIELS INTEGRATION BOURSIERE
	AMADEUS FRANCE SERVICES
	ALTEN SYSTEMES D'INFORMATIONS NORD
	ASSISTANCE-CONSEIL-TECHNIQUE-INFORMATIQU
	TRANSICIEL INFOGERANCE SYSTEMES RESEAUX

Les entreprises non utilisées dans la recherche : 

Nombre de bases crées : 425
Nombre de bases explorées : 425
Profondeur : 20

Solution numéro 1

Les bases trouvés : 
	Base_23.txt : 43
	Base_10.txt : 31

Les entreprises trouvés : 
	ADONIX APPLICATIONS ET SERVICES trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
	MEDASYS INFRASTRUCTURE SERVICES trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
		Base_10.txt : 31
	CONTINUITY SERVICES FRANCE trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
	FRANCE TELECOM EXPERTISE ET SERVICE trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
	EDS GLOBAL FIELD SERVICES FRANCE trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
	SERVICES LOGICIELS INTEGRATION BOURSIERE trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
	AMADEUS FRANCE SERVICES trouvé dans le ou les bases suivantes : 
		Base_23.txt : 43
	ALTEN SYSTEMES D'INFORMATIONS NORD trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31
	ASSISTANCE-CONSEIL-TECHNIQUE-INFORMATIQU trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31
	TRANSICIEL INFOGERANCE SYSTEMES RESEAUX trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31

Cout de la solution : 74
Temps de résolution : 356 ms


Scénario scenario2

Cout total : 955

Les bases utilisées dans la recherche : 
	Base_22.txt ; Cout = 88 ; Nombre d'entreprises = 65
	Base_01.txt ; Cout = 87 ; Nombre d'entreprises = 29
	Base_11.txt ; Cout = 84 ; Nombre d'entreprises = 20
	Base_05.txt ; Cout = 82 ; Nombre d'entreprises = 75
	Base_06.txt ; Cout = 81 ; Nombre d'entreprises = 57
	Base_18.txt ; Cout = 80 ; Nombre d'entreprises = 31
	Base_08.txt ; Cout = 75 ; Nombre d'entreprises = 27
	Base_12.txt ; Cout = 69 ; Nombre d'entreprises = 15
	Base_09.txt ; Cout = 58 ; Nombre d'entreprises = 19
	Base_16.txt ; Cout = 53 ; Nombre d'entreprises = 51
	Base_03.txt ; Cout = 43 ; Nombre d'entreprises = 23
	Base_19.txt ; Cout = 42 ; Nombre d'entreprises = 27
	Base_13.txt ; Cout = 36 ; Nombre d'entreprises = 29
	Base_10.txt ; Cout = 31 ; Nombre d'entreprises = 41
	Base_02.txt ; Cout = 26 ; Nombre d'entreprises = 85
	Base_07.txt ; Cout = 20 ; Nombre d'entreprises = 56

Les bases non utilisées dans la recherche : 
	Base_14.txt : 9
	Base_20.txt : 78

Les entreprises utilisées dans la recherche : 
	ALSTOM INFORMATION TECHNOLOGY CENTRE
	ASSISTANCE GENIE LOGICIEL
	ATOS ORIGIN INFOGERANCE
	ARINSO FRANCE
	ALCANET INTERNATIONAL
	BASE D INFORMATIONS LEGALES

Les entreprises non utilisées dans la recherche : 

Nombre de bases crées : 534
Nombre de bases explorées : 534
Profondeur : 17

Solution numéro 1

Les bases trouvés : 
	Base_10.txt : 31
	Base_02.txt : 26

Les entreprises trouvés : 
	ALSTOM INFORMATION TECHNOLOGY CENTRE trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31
	ASSISTANCE GENIE LOGICIEL trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26
	ATOS ORIGIN INFOGERANCE trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31
		Base_02.txt : 26
	ARINSO FRANCE trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26
	BASE D INFORMATIONS LEGALES trouvé dans le ou les bases suivantes : 
		Base_10.txt : 31
	ALCANET INTERNATIONAL trouvé dans le ou les bases suivantes : 
		Base_02.txt : 26

Cout de la solution : 57
Temps de résolution : 103 ms


Scénario scenario3

Cout total : 473

Les bases utilisées dans la recherche : 
	Base_22.txt ; Cout = 88 ; Nombre d'entreprises = 65
	Base_01.txt ; Cout = 87 ; Nombre d'entreprises = 29
	Base_06.txt ; Cout = 81 ; Nombre d'entreprises = 57
	Base_16.txt ; Cout = 53 ; Nombre d'entreprises = 51
	Base_19.txt ; Cout = 42 ; Nombre d'entreprises = 27
	Base_13.txt ; Cout = 36 ; Nombre d'entreprises = 29
	Base_10.txt ; Cout = 31 ; Nombre d'entreprises = 41
	Base_02.txt ; Cout = 26 ; Nombre d'entreprises = 85
	Base_07.txt ; Cout = 20 ; Nombre d'entreprises = 56
	Base_14.txt ; Cout = 9 ; Nombre d'entreprises = 25

Les bases non utilisées dans la recherche : 
	Base_21.txt : 83
	Base_03.txt : 43

Les entreprises utilisées dans la recherche : 
	ADP FRANCE
	A.G.F. - INFORMATIQUE
	AIRIAL CONSEIL
	ALDATA SOLUTION

Les entreprises non utilisées dans la recherche : 

Nombre de bases crées : 839
Nombre de bases explorées : 839
Profondeur : 8

Solution numéro 1

Les bases trouvés : 
	Base_13.txt : 36
	Base_19.txt : 42
	Base_14.txt : 9
	Base_07.txt : 20

Les entreprises trouvés : 
	ADP FRANCE trouvé dans le ou les bases suivantes : 
		Base_07.txt : 20
	A.G.F. - INFORMATIQUE trouvé dans le ou les bases suivantes : 
		Base_14.txt : 9
	AIRIAL CONSEIL trouvé dans le ou les bases suivantes : 
		Base_13.txt : 36
	ALDATA SOLUTION trouvé dans le ou les bases suivantes : 
		Base_19.txt : 42

Cout de la solution : 107
Temps de résolution : 156 ms